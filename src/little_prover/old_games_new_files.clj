(ns little-prover.old-games-new-files
  (:use [little-prover.scheme]))

(car (cons 'ham '(eggs)))
;; ham

(car (cons 'ham '(cheese)))
;; ham

(car (cdr (cons 'eggs '(ham))))
;; ham

(car (cons (car '(ham)) '(eggs)))
;; ham

(car (cons 'ham '(eggs)))
;; ham

(atom? '())
;; true
