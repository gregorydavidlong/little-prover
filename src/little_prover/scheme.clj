(ns little-prover.scheme)

;; Keybindings: (see http://cider.readthedocs.io/en/latest/interactive_programming/)
;;
;; M-x cider-jack-in  - Start REPL
;; C-c C-l            - Load file
;; C-c C-e            - Evaluate
;; C-c M-n            - Switch to this namespace in the REPL
;; C-x M-p            - Load the expression into the REPL
;; C-c C-v w          - Replace expression with result

(defn atom?
  [a]
  (or (= '() a)
      ((complement coll?) a)))

(defn car
  [l]
  (first l))

(defn cdr
  [l]
  (next l))
